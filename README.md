# Setup

- Follow the [building instructions](https://github.com/probe-rs/probe-rs) of probe-rs
- Install flip-link as a stack overflow protection mechanism:
```
cargo install flip-link
```
- Install probe-run using the following command:
```
cargo install probe-run
```
- As an alternative (or addition) to probe-run, cargo flash can be used:
```
cargo install cargo-flash
```

# Running the project

You can run the project in debug mode as follows:
```
cargo run
```

Or in release mode:

```
cargo run --release
```

## Flashing binary to the chip

The binary can also be flashed to the chip.
Make sure to change the chip argument if you are using a different chip!

```
cargo flash --release --chip stm32f411vetx
```

## Checking the size of binary file:

System V binary format specification (can be installed following instructions here: https://github.com/rust-embedded/cargo-binutils):

```
cargo size --bin blink -- -A
```

# Troubleshooting

- Permission denied error while trying to flash: https://stackoverflow.com/questions/23312087/error-3-opening-st-link-v2-device
