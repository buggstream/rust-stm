#![no_main]
#![no_std]

use rust_stm as _;
// global logger + panicking-behavior + memory layout
use stm32f4xx_hal as _;
use stm32f4xx_hal::{delay::Delay, prelude::*, stm32};

#[cortex_m_rt::entry]
fn main() -> ! {
    let dp = stm32::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();

    let rcc = dp.RCC.constrain();

    // Configure clock to 100 MHz (i.e. the maximum) and freeze it
    let clocks = rcc.cfgr.sysclk(100.mhz()).freeze();
    let mut delay = Delay::new(cp.SYST, clocks);
    let gpiod = dp.GPIOD.split();

    let green_led = gpiod.pd12.into_push_pull_output().downgrade();
    let orange_led = gpiod.pd13.into_push_pull_output().downgrade();
    let red_led = gpiod.pd14.into_push_pull_output().downgrade();
    let blue_led = gpiod.pd15.into_push_pull_output().downgrade();
    let mut leds = [green_led, orange_led, red_led, blue_led];

    let mut counter = 0_usize;
    let delay_ms = 200_u32;

    loop {
        let tail = (counter + leds.len() - 2) % leds.len();
        leds[tail].set_low().unwrap();
        leds[counter].set_high().unwrap();

        counter = (counter + 1) % leds.len();

        delay.delay_ms(delay_ms);
    }
}
