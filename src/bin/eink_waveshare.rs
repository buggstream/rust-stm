#![no_main]
#![no_std]

use rust_stm as _;
// global logger + panicking-behavior + memory layout
use embedded_graphics::fonts::{Font12x16, Font24x32, Text};
use embedded_graphics::pixelcolor::BinaryColor;
use embedded_graphics::prelude::*;
use embedded_graphics::primitives::{Line, Rectangle};
use embedded_graphics::style::{PrimitiveStyle, TextStyleBuilder};
use epd_waveshare::epd1in54c::{Display1in54c, EPD1in54c};
use epd_waveshare::prelude::*;
use stm32f4xx_hal as _;
use stm32f4xx_hal::hal::blocking::spi::Write;
use stm32f4xx_hal::hal::digital::v2::{InputPin, OutputPin};
use stm32f4xx_hal::spi::{NoMiso, Spi};
use stm32f4xx_hal::{delay::Delay, prelude::*, stm32};

const SCREEN_WIDTH: i32 = epd_waveshare::epd1in54c::WIDTH as i32;

#[cortex_m_rt::entry]
fn main() -> ! {
    let dp = stm32::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();

    let rcc = dp.RCC.constrain();

    // Configure clock to 100 MHz (i.e. the maximum) and freeze it
    let clocks = rcc.cfgr.sysclk(100.mhz()).freeze();
    let mut delay = Delay::new(cp.SYST, clocks);
    let gpioa = dp.GPIOA.split();

    let clock = gpioa.pa5.into_alternate_af5();
    let mosi = gpioa.pa7.into_alternate_af5();
    let cs = gpioa.pa3.into_push_pull_output();
    let dc = gpioa.pa2.into_push_pull_output();
    let rst = gpioa.pa1.into_push_pull_output();
    let busy = gpioa.pa0.into_floating_input();

    let mut spi = Spi::spi1(
        dp.SPI1,
        (clock, NoMiso, mosi),
        SPI_MODE,
        4.mhz().into(),
        clocks,
    );
    let mut epd = EPD1in54c::new(&mut spi, cs, busy, dc, rst, &mut delay).unwrap();
    draw(&mut spi, &mut epd).unwrap();

    loop {}
}

fn draw<SPI, CS, BUSY, DC, RST>(
    spi: &mut SPI,
    epd: &mut EPD1in54c<SPI, CS, BUSY, DC, RST>,
) -> Result<(), SPI::Error>
where
    SPI: Write<u8>,
    CS: OutputPin,
    BUSY: InputPin,
    DC: OutputPin,
    RST: OutputPin,
{
    epd.clear_frame(spi)?;
    epd.display_frame(spi)?;

    let mut mono_display = Display1in54c::default();
    let mut chromatic_display = Display1in54c::default();

    let mut rust_title = Text::new("Rust", Point::new(0, 32)).into_styled(
        TextStyleBuilder::new(Font24x32)
            .text_color(BinaryColor::On)
            .background_color(BinaryColor::Off)
            .build(),
    );

    rust_title = center_width(rust_title, SCREEN_WIDTH);
    rust_title.draw(&mut mono_display).unwrap();

    let mut subtext = Text::new("WaveShare", Point::new(0, 77)).into_styled(
        TextStyleBuilder::new(Font12x16)
            .text_color(BinaryColor::On)
            .background_color(BinaryColor::Off)
            .build(),
    );

    subtext = center_width(subtext, SCREEN_WIDTH);
    subtext.draw(&mut chromatic_display).unwrap();

    Rectangle::new(Point::new(15, 15), Point::new(SCREEN_WIDTH - 15, 115))
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 2))
        .draw(&mut mono_display)
        .unwrap();

    Line::new(Point::new(20, 97), Point::new(SCREEN_WIDTH - 20, 97))
        .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 4))
        .draw(&mut chromatic_display)
        .unwrap();

    defmt::info!("Buffer updated");

    // Display updated frame
    epd.update_color_frame(spi, mono_display.buffer(), chromatic_display.buffer())?;
    defmt::info!("Updated frame.");
    epd.display_frame(spi)?;
    defmt::info!("Finished displaying frame.");

    // Set the EPD to sleep
    defmt::info!("Sleeping...");
    epd.sleep(spi)?;

    Ok(())
}

fn center_width<E: Dimensions + Transform>(element: E, screen_width: i32) -> E {
    let width_offset = (screen_width - (element.size().width as i32)) / 2;

    element.translate(Point::new(width_offset as i32, 0))
}
