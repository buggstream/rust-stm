#![no_main]
#![no_std]

use rust_stm as _;
// global logger + panicking-behavior + memory layout
use stm32f4xx_hal as _;
use stm32f4xx_hal::{prelude::*, stm32};

#[cortex_m_rt::entry]
fn main() -> ! {
    let dp = stm32::Peripherals::take().unwrap();
    let rcc = dp.RCC.constrain();
    rcc.cfgr.sysclk(100.mhz()).freeze();

    enable_gpiod();

    into_push_pull_output(12);
    into_push_pull_output(13);
    into_push_pull_output(14);
    into_push_pull_output(15);

    defmt::info!("Is high: {:bool}", is_high(12));

    set_high(12);

    defmt::info!("Is high: {:bool}", is_high(12));

    set_high(13);
    set_high(14);
    set_high(15);

    loop {}
}

fn into_push_pull_output(pin: u8) {
    let gpiod_addr = gpiod_address_block();
    let offset = pin as i32 * 2;

    gpiod_addr
        .pupdr
        .modify(|r, w| unsafe { w.bits((r.bits() & !(0b11 << offset)) | (0b00 << offset)) });
    gpiod_addr
        .otyper
        .modify(|r, w| unsafe { w.bits(r.bits() & !(0b1 << pin)) });
    gpiod_addr
        .moder
        .modify(|r, w| unsafe { w.bits((r.bits() & !(0b11 << offset)) | (0b01 << offset)) });
}

fn set_high(pin: u8) {
    let gpiod_addr = gpiod_address_block();
    unsafe { gpiod_addr.bsrr.write(|w| w.bits(1 << pin)) };
}

fn is_high(pin: u8) -> bool {
    let gpiod_addr = gpiod_address_block();
    let masked = gpiod_addr.odr.read().bits() & (1 << pin);

    masked > 0
}

#[inline]
fn gpiod_address_block() -> &'static stm32::gpioh::RegisterBlock {
    unsafe { &*stm32::GPIOD::ptr() }
}

fn enable_gpiod() {
    let rcc_ptr = stm32::RCC::ptr();
    unsafe { &(*rcc_ptr).ahb1enr.write(|w| w.gpioden().set_bit()) };
}
